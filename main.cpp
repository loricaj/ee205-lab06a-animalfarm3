///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Joshua Loricaj <loricaj@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   3/23/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {

   srand(time(0));

/*   for( int i = 0 ; i < 25 ; i++ ) {
      Animal* a = AnimalFactory::getRandomAnimal();
      cout << a->speak() << endl;
   } */

   cout << "Welcome to Animal Farm 3" << endl;

   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   for(int i = 0 ; i<25 ; i++ ) { 
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }

   cout << endl;
   cout << "Array of Animals" << endl;
   cout << "  Is is empty: " << boolalpha << animalArray.empty() << endl;
   cout << "  Number of elements: " << animalArray.size() << endl;
   cout << "  Max size: " << animalArray.max_size() << endl;

   for(int i = 0 ; i<25 ; i++){
      cout << animalArray[i]->speak() << endl;
   }

   for(Animal* animal : animalArray){
      if(animal != NULL)
        delete animal;   
   }

   list<Animal*> animalList;
   for (int i = 0 ; i < 25 ; i++ ) {
      animalList.push_front( AnimalFactory::getRandomAnimal() );
   }

   cout << endl;
   cout << "List of Animals" << endl;
   cout << "  Is is empty: " << boolalpha << animalList.empty() << endl;
   cout << "  Number of elements: " << animalList.size() << endl;
   cout << "  Max size: " << animalList.max_size() << endl;

   for(Animal* animal : animalList) {
      cout << animal->speak() << endl;
   }

   for(Animal* animal : animalList){
      if(animal != NULL)
        delete animal;   
   }

	return 0;
}
