///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/23/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool newNative, enum Color newColor, enum Gender newGender ) { 
   gender = newGender;
   species = "Fistularia chinesis";
   scaleColor = newColor;
   favoriteTemp = 80.6;
   isNative = newNative; 
}

void Nunu::printInfo() {
   cout << "Nunu" << endl; 
   cout << "   Is native = [" << boolalpha << isNative << "]" << endl;
   Fish::printInfo();
}

} // nmespace animalfarm

