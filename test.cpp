/*
 * This is a program to test new utility functions 
 */

#include <iostream>
#include <cstdlib>
#include <cstring>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace animalfarm;
using namespace std;

static const enum Gender getRandomGender();
static const enum Color getRandomColor();
static const bool getRandomBool();
static const float getRandomWeight( const float from, const float to );
static const string getRandomName();

int main() {

   srand(time(0));

   cout << "Random Gender: " << getRandomGender() << endl;
   cout << "Random Color: " << getRandomColor() << endl;
   cout << "Random Boolean: " << getRandomBool() << endl;
   cout << "Random Weight: " << getRandomWeight(0,5) << endl;
   cout << "Random Name: " << getRandomName() << endl;

   for(Animal* animal : animalArray) {
      if(animal != NULL)
         delete animal;
   }

   return 0;
}

///////////////////////////////////////////////////////////////////////////

const enum Gender getRandomGender(){
   Gender randomGender = Gender(rand()%3);
   return randomGender;
}

const enum Color getRandomColor(){
   Color randomColor = Color(rand()%6);
   return randomColor;
}

const bool getRandomBool(){
   bool randomBool = rand()%2;
   return randomBool;
}

const float getRandomWeight(const float from, const float to){
   float random = ( (float) rand() ) / (float) RAND_MAX;
   float diff = to - from;
   float randomWeight = random * diff;
   return from + randomWeight;
}

const string getRandomName() {
   string randomName; 
   int length = ( rand()%6 ) + 4;

   char randomChar = rand()%26 + 65; 
   randomName += randomChar;

   for(int i=1;i<length;i++){
      randomChar = ( rand()%26 ) + 97;
      randomName += randomChar;
   }
   return randomName;
}
