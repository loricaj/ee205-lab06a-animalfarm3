///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   3/23/21
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK,WHITE,RED,SILVER,YELLOW,BROWN };

class Animal {
public:
	enum Gender gender;
	string      species;

	virtual const string speak() = 0;
	
	void printInfo();
	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);

   static const enum Gender getRandomGender();
   static const enum Color getRandomColor();
   static const bool getRandomBool();
   static const float getRandomWeight( const float from, const float to );
   static const string getRandomName();

   Animal();
   ~Animal();
};

class AnimalFactory {
public:
   static Animal* getRandomAnimal();
};

} // namespace animalfarm
